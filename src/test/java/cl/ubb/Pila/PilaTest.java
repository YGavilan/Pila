package cl.ubb.Pila;

import static org.junit.Assert.*;

import org.junit.Test;

public class PilaTest {
	
    @Test
    public void IngresarPilaVacia() { //Nueva Stack es vac�a
    	Pila stack = new Pila(0);
    	assertEquals(true,stack.esVacia());	
    }
    public void IngresaUno_PilaNoVacia() { 
    	Pila stack = new Pila(1);
        stack.push(1);
        assertEquals(false,stack.esVacia());	
    }
  
    @Test
    public void Ingresa1y2_StackNoVacia() { 
    	Pila stack = new Pila(2);
        stack.push(1);
        stack.push(2);
        assertEquals(false,stack.esVacia());	
    }
    
    @Test
    public void Ingresa1y2_Length2() { 
    	Pila stack = new Pila(2);
        stack.push(1);
        stack.push(2);
        assertEquals(2,stack.tamano);	
    }
    
    @Test
    public void Ingresa1_Pop_1() { 
    	Pila stack = new Pila(2);
        stack.push(1);
        String elemento = stack.elementos[stack.tamano-1];
        stack.pop();
        assertEquals(1,elemento);	
    }
    
    @Test
    public void Ingresa1y2_Pop_2() { //Al agregar n�meros 1 y 2 hacer pop,  stack devuelve el n�mero 2.
    	Pila stack = new Pila(2);
        stack.push(1);
        stack.push(2);
        String elemento = stack.elementos[stack.tamano-1];
        stack.pop();
        assertEquals(2,elemento);	
    }
    
    @Test
    public void Ingresa3y4_Pop_Pop_4() { //Al agregar n�meros 1 y 2 hacer pop,  stack devuelve el n�mero 2.
    	Pila stack = new Pila(2);
        stack.push(3);
        stack.push(4);
        Pila aux = new Pila(2);
        for(int i=0; i <= stack.tamano;i++){
        aux.push(stack.elementos[i]);
        }
        stack.pop();
        stack.pop();
        assertEquals(aux,stack);	
    }
    
    
}
